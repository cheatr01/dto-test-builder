package dtotestbuilder;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.ModifierContributor;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.dynamic.DynamicType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

public class DefaultInvocationHandler<T> implements InvocationHandler {

	private Class<T> clazz;

	private T instance;

	public DefaultInvocationHandler(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
		if (method.getName().equals("build")) {
			return getInstance();
		}

		final Optional<Field> fieldOpt = Arrays.stream(clazz.getDeclaredFields()).filter(f -> f.getName().equals(method.getName())).findFirst();
		if (fieldOpt.isPresent()) {
			final T dto = getInstance();
			final Field field = fieldOpt.get();
			final String fieldName = field.getName();
			final Method setter = clazz.getDeclaredMethod(generateSetterMethodName(fieldName));
			setter.invoke(dto, objects);
		}

		throw new RuntimeException("Bad name");
	}

	private String generateSetterMethodName(String fieldName) {
		return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	private T getInstance() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if (instance == null) {
			return createInstance();
		}
		return instance;
	}

	private T createInstance() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		instance = (T) clazz.getConstructor().newInstance();
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getType().equals(String.class)) {
				field.setAccessible(true);
				field.set(instance, createDefaultString(field.getName()));
			}
		}
		return instance;
	}

	private String createDefaultString(String fieldName) {
		return "dummy" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}
}
