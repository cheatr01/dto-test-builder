package dtotestbuilder;

public class TestDto {

	private String testString;

	public TestDto() {
	}

	public String getTestString() {
		return testString;
	}

	public void setTestString(String testString) {
		this.testString = testString;
	}
}
