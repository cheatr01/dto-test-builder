package dtotestbuilder;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

public class TestBuilderFactory {

	public interface DtoBuilder {
		<T> T build();

		<T> DtoBuilder setValueToField(String fieldName, T value);
	}

	/*public <T, I> I getBuilder(Class<T> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		final Constructor<T> constructor = clazz.getConstructor();
		final T instance = constructor.newInstance();

		final TypeDescription.Latent builderType = new TypeDescription.Latent(clazz.getSimpleName() + "Builder", 0, null, (List) null);
		final DynamicType.Unloaded<> builderClass = new ByteBuddy()
				.subclass(builderType)
				.defineMethod("build", clazz)
				.intercept(FixedValue.reference(instance))
				.make();

		return builderClass.load(builderClass.getClass().getClassLoader()).getLoaded().cast(builderType.getC)getConstructor()newInstance();
	}*/

	public <T, B extends DtoBuilder> B getProxyBuilder(Class<T> clazz) {
		final ByteBuddy byteBuddy = new ByteBuddy();
		DynamicType.Builder<TestBuilderFactory.DtoBuilder> newInterface = byteBuddy.subclass(TestBuilderFactory.DtoBuilder.class);
		Arrays.stream(clazz.getDeclaredFields())
				.forEach(field -> newInterface.defineMethod(generateSetterMethodName(field.getName()), Void.class, 0));
		final DynamicType.Builder<DtoBuilder> name = newInterface.name(clazz.getPackageName() + "." + clazz.getName() + "Builder");
		final DynamicType.Unloaded<DtoBuilder> dtoBuilderUnloaded = name.make();
		final DynamicType.Loaded<DtoBuilder> dynamicType = dtoBuilderUnloaded.load(clazz.getClassLoader());
		return (B) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{dynamicType.getLoaded()}, new DefaultInvocationHandler(clazz));
	}


	private String generateSetterMethodName(String fieldName) {
		return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}
}
